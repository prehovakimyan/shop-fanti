@extends('layouts.app')

@section('content')
    <div class="big_top">
        <div class="big_top_center">
            <div class="slider_left">
                <div class="video_left_top">
                    <div class="video_left_top_year">
                        <b>12</b>
                        <i>Лет работы</i>
                        <span> с чистой деловой репутацией</span>
                    </div>
                    <div class="video_left_top_year">
                        <b>35</b>
                        <i>Единиц автотранспорта</i>
                        <span>выполняют доставку ежедневно</span>
                    </div>
                    <div class="video_left_top_year">
                        <b>9 000</b>
                        <i>Тонн металла</i>
                        <span>постоянно на складе</span>
                    </div>

                </div>
                <div class="video_left_bottom">
                    <a href="https://www.youtube.com/watch?v=v0ECkY8vaNc" class="video-button d-none d-lg-flex" data-fancybox="" title="Посмотреть видео">
                        <div class="play_left">
                            <div class="play_left_img">
                                <img src="/images/play_back.webp" width="119" height="89" alt="Видео">
                            </div>
                            <img class="select_pllay" src="/images/play.png" width="40" height="40" alt="Видео">
                        </div>
                        <div class="play_right">
                            Узнайте о нашей компании<br>
                            <i>0:44</i>
                        </div>
                    </a>
                </div>
            </div>
            <div class="slider_right">
                <div id="splide1" class="splide splide--slide splide--ltr splide--draggable is-active is-initialized" role="region" aria-roledescription="carousel">
                    <div class="splide__arrows splide__arrows--ltr"><button class="splide__arrow splide__arrow--prev" type="button" aria-label="Previous slide" aria-controls="splide1-track" fdprocessedid="p1wnpd"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" width="40" height="40" focusable="false"><path d="m15.5 0.932-4.3 4.38 14.5 14.6-14.5 14.5 4.3 4.4 14.6-14.6 4.4-4.3-4.4-4.4-14.6-14.6z"></path></svg></button><button class="splide__arrow splide__arrow--next" type="button" aria-label="Next slide" aria-controls="splide1-track" fdprocessedid="wckdjt" disabled=""><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" width="40" height="40" focusable="false"><path d="m15.5 0.932-4.3 4.38 14.5 14.6-14.5 14.5 4.3 4.4 14.6-14.6 4.4-4.3-4.4-4.4-14.6-14.6z"></path></svg></button></div><div class="splide__track splide__track--slide splide__track--ltr splide__track--draggable" id="splide1-track" style="padding-left: 0px; padding-right: 0px;" aria-live="off" aria-atomic="true" aria-busy="false">
                        <ul class="splide__list" id="splide1-list" role="presentation" style="transform: translateX(-3760px);">
                            <li class="splide__slide" id="splide1-slide01" role="tabpanel" aria-roledescription="slide" aria-label="1 of 5" style="width: calc(100%);" aria-hidden="true">
                                <img class=" ls-is-cached lazyloaded" data-src="https://robmetalstal.ru/images/slider/1679742713-555.webp" width="940" height="380" src="https://robmetalstal.ru/images/slider/1679742713-555.webp" alt="Молстрой">
                            </li>
                            <li class="splide__slide" id="splide1-slide02" role="tabpanel" aria-roledescription="slide" aria-label="2 of 5" style="width: calc(100%);" aria-hidden="true">
                                <img class=" ls-is-cached lazyloaded" data-src="https://robmetalstal.ru/images/slider/1680864238-555.webp" width="940" height="380" src="https://robmetalstal.ru/images/slider/1680864238-555.webp" alt="Молстрой">
                            </li>
                            <li class="splide__slide" id="splide1-slide03" role="tabpanel" aria-roledescription="slide" aria-label="3 of 5" style="width: calc(100%);" aria-hidden="true">
                                <a href="https://robmetalstal.ru/mangaly-dlya-dachi" title="Подробнее" tabindex="-1">	<img class=" ls-is-cached lazyloaded" data-src="https://robmetalstal.ru/images/slider/1680868284-ffff.webp" width="940" height="380" src="https://robmetalstal.ru/images/slider/1680868284-ffff.webp" alt="Молстрой">
                                </a>	</li>
                            <li class="splide__slide is-prev" id="splide1-slide04" role="tabpanel" aria-roledescription="slide" aria-label="4 of 5" style="width: calc(100%);" aria-hidden="true">
                                <a href="https://robmetalstal.ru/rashodnye-materialy/elektrody" title="Подробнее" tabindex="-1">	<img class=" ls-is-cached lazyloaded" data-src="https://robmetalstal.ru/images/slider/1679738470-нотрон.webp" width="940" height="380" src="https://robmetalstal.ru/images/slider/1679738470-нотрон.webp" alt="Молстрой">
                                </a>	</li>
                            <li class="splide__slide is-active is-visible" id="splide1-slide05" role="tabpanel" aria-roledescription="slide" aria-label="5 of 5" style="width: calc(100%);">
                                <img class=" ls-is-cached lazyloaded" data-src="https://robmetalstal.ru/images/slider/1679738461-скидка.webp" width="940" height="380" src="https://robmetalstal.ru/images/slider/1679738461-скидка.webp" alt="Молстрой">
                            </li>
                        </ul>
                    </div>
                    <ul class="splide__pagination splide__pagination--ltr" role="tablist" aria-label="Select a slide to show"><li role="presentation"><button class="splide__pagination__page" type="button" role="tab" aria-controls="splide1-slide01" aria-label="Go to slide 1" fdprocessedid="m5ek1" tabindex="-1"></button></li><li role="presentation"><button class="splide__pagination__page" type="button" role="tab" aria-controls="splide1-slide02" aria-label="Go to slide 2" fdprocessedid="kr4lmo" tabindex="-1"></button></li><li role="presentation"><button class="splide__pagination__page" type="button" role="tab" aria-controls="splide1-slide03" aria-label="Go to slide 3" fdprocessedid="d3u3gh" tabindex="-1"></button></li><li role="presentation"><button class="splide__pagination__page" type="button" role="tab" aria-controls="splide1-slide04" aria-label="Go to slide 4" fdprocessedid="sb8b5" tabindex="-1"></button></li><li role="presentation"><button class="splide__pagination__page is-active" type="button" role="tab" aria-controls="splide1-slide05" aria-label="Go to slide 5" aria-selected="true" fdprocessedid="iwxfm"></button></li></ul></div>
            </div>
        </div>
    </div>
@endsection


