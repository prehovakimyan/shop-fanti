<?php

namespace App\Http\Controllers;

use App\Models\Dictionary;
use Illuminate\Http\Request;

class DictionaryController extends Controller
{
    public function getTranslate($short_code,$lang){
        $get_glssary = Dictionary::select("value_$lang as translate")->where('short_code','=', $short_code)->first();

        if ($get_glssary) {
            return $get_glssary->translate;
        }
        else {
            return "*_".$short_code."_*";
        }
    }
}
