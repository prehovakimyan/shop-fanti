<?php

namespace App\Http\Controllers;

use App\Models\Language;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index(Request $request) {

        $first_lang = Language::where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        return view('home', compact( 'lang'));
    }

}
